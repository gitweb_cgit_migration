<VirtualHost *:80>
	ServerName      git.openezx.org

	ErrorLog	/var/log/apache2/git.openezx.org/error.log
	CustomLog	/var/log/apache2/git.openezx.org/access.log common

	#
	# Redirects mapping gitweb -> cgit
	# Inspired from: http://www.clearchain.com/blog/posts/cgit-upgrade-gitweb-retired
	#
	# Gitweb uses get targets seperated by ;
	#
	# /?...;...;...
	#
	# p = Project
	# a = Action like (
	#       blob,
	#       blob_plain,
	#       commitdiff,
	#       commit,
	#       history,
	#       log,
	#       rss|atom,
	#       shortlog,
	#       summary,
	#       tag,
	#       tree,
	#       snapshot
	#     )
	# h = SHA Hash
	# hb= SHA Hash Tree Base
	# f= file/dir
	# sf= snapshot format
	#
	# Cgit uses the following:
	#
	# /project/action/?...
	#
	# action ( commit, log, diff, tree, tag, patch )
	#
	# id = SHA Hash
	# id2 = SHA Hash
	# h = head
	#
	# Translation rules
	#
	# Project is a straight redirect
	# ---
	# /?p=(.+)\.git;       http://cgit.openezx.org/$1/
	#
	# Action requires a mapping
	# ---
	# a=(blob|tree)                 /tree/
	# a=(blobdiff|commitdiff)       /diff/
	# a=commit                      /commit/
	# a=(summary)                   /
	# a=(shortlog|log|history)      /log/
	# a=tag                         /tag/
	# a=blob_plain                  /blob/
	# a=rss|atom                    /atom
	#
	# Targets require mapping
	# ---
	# h=(.+)     id=$1
	# f=(.+)     /$1
	# hb=(.+)    id2=$1
	#
	# Now putting it all together
	#
	RewriteEngine On
	#RewriteLog /tmp/rewrite.log
	#RewriteLogLevel 5

	# blob
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/blob/(.+)/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=blob;h=(.+);hb=(.+);f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/tree/%5?id=%3;id2=%4 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/blob/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=blob;hb=(.+);f=(.+)
	RewriteRule ^/.*$  http://cgit.openezx.org/%1/tree/%4?id=%3 [R,L,NE]

	RewriteCond %{query_string} p=(.+)(\.git);a=blob;f=(.+);h=(.+);hb=(.+)
	RewriteRule ^/.*$  http://cgit.openezx.org/%1/tree/%3?id=%4;id2=%5 [R,L,NE]

	RewriteCond %{query_string} p=(.+)(\.git);a=blob;f=(.+);h=(.+)
	RewriteRule ^/.*$  http://cgit.openezx.org/%1/tree/%3?id=%4 [R,L,NE]

	# tree
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/tree/(.+)/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=tree;h=(.+);hb=(.+);f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/tree/%5?id=%4 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/tree/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=tree;hb=(.+);f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/tree/%4?id=%3 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/tree/(.+)/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=tree;h=(.+);hb=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/tree/?id=%4 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/tree/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=tree;hb=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/tree/?id=%3 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/tree [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=tree
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/tree/? [R,L,NE]

	# commitdiff
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/commitdiff/(.+)/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=blobdiff;h=(.+);hp=(.+);hb=(.+);f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/diff/%6?id2=%4;id=%3;id3=%5 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/commitdiff/(.+)/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=commitdiff;h=(.+);hp=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/diff/?id=%4;id2=%3 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/commitdiff/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=commitdiff;h=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/diff/?id=%3 [R,L,NE]

	# commit
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/commit/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=commit;h=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/commit/?id=%3 [R,L,NE]

	# summary
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/summary [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=summary
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/? [R,L,NE]

	# shortlog 
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/shortlog/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=shortlog;h=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/?id=%3 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/shortlog [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=shortlog
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/? [R,L,NE]

	# log
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/log/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=log;h=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/?id=%3 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/log [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=log
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log? [R,L,NE]

	# history
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/history/(.+)/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=history;h=(.+);hb=(.+);f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/%5?id=%4 [R,L,NE]

	RewriteCond %{query_string} p=(.+)(\.git);a=history;f=(.+);h=(.+);hb=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/%3?id=%4;id2=%5 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/history/(.+):/(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/%4?id=%3 [R,L,NE]

	RewriteCond %{query_string} p=(.+)(\.git);a=history;f=(.+);h=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/%3?id=%4 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/history/(.+)/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=history;h=(.+);hb=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/?id=%4 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/history/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=history;hb=(.+);f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/%4?id=%3 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/history/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=history;hb=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/log/?id=%3 [R,L,NE]

	# tag
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/tag/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=tag;h=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/tag/?id=%3 [R,L,NE]

	# blob_plain
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/blob_plain/(.+):/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=blob_plain;h=(.+);f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/plain/%4?id=%3 [R,L,NE]

	RewriteCond %{query_string} p=(.+)(\.git);a=blob_plain;f=(.+);hb=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/plain/%3?id2=%4 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/blob_plain/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=blob_plain;f=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/plain/%3 [R,L,NE]

	# rss|atom
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/(rss|atom)/refs/heads/(.+) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=(rss|atom);h=refsheads/(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/atom?h=%4 [R,L,NE]

	RewriteCond %{REQUEST_URI} /(.+)(\.git)/(rss|atom) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=(rss|atom)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/atom? [R,L,NE]

	# snapshot
	RewriteCond %{REQUEST_URI} /(.+)(\.git)/snapshot/(.+)(\.tar\.gz|\.tar\.bz2) [OR]
	RewriteCond %{query_string} p=(.+)(\.git);a=snapshot;h=(.+);sf=(.+)
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/snapshot/%3.tar.gz [R,L,NE]

	# Fail safes incase nothing above matches, try at least to put the person in the project
	RewriteCond %{REQUEST_URI} /(.+)\.git.* [OR]
	RewriteCond %{query_string} p=(.+)\.git.*
	RewriteRule ^/.*$ http://cgit.openezx.org/%1/? [R,L,NE]

	# Or else in the root of cgit
	RewriteRule ^.* http://cgit.openezx.org/ [R,L,NE]
</VirtualHost>
